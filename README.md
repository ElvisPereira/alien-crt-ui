# Alien CRT UI

This is a fork of the awesome [Alien RPG CRT UI](https://github.com/FranzWarm/AlienCRT_UI) created by FranzWarm for the [Alien RP System](https://foundryvtt.com/packages/alienrpg) for [Foundry VTT](https://foundryvtt.com/).

Since Franz can't continue updating the module, and it stopped working on the new version of the system, I decided to fork and update it.

If you encounter any problem with this module, please open a new [issue](https://gitlab.com/ElvisPereira/alien-crt-ui/-/issues).
