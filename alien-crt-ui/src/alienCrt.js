/* global Hooks, game, Actors, $ */

import alienrpgActorSheet from '../../../systems/alienrpg/module/actor/actor-sheet.js';

export class crtActorSheet extends alienrpgActorSheet {
  /**
   * Path to the HTML template file used to render the inner content.
   * @override
   */
  get template () {
    const path = 'modules/alien-crt-ui/static/templates/actor/';
    return `${path}/${this.actor.data.type}-sheet.html`;
  }
}

Hooks.once('ready', () => {
  game.settings.register('crtUi', 'crtEffect', {
    name: game.i18n.localize('CRTUI.crtEffect'),
    hint: game.i18n.localize('CRTUI.crtEffectHint'),
    scope: 'client',
    config: true,
    default: true,
    type: Boolean
  });

  Actors.registerSheet('alienCrt', crtActorSheet, {
    types: ['character', 'creature', 'synthetic', 'territory', 'vehicles'],
    makeDefault: true
  });
});

Hooks.on('rendercrtActorSheet', () => {
  if (game.settings.get('crtUi', 'crtEffect')) {
    $('.actor-sheet').addClass('crt-effect');
  }
});
